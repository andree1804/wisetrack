from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views import View
from django.http import HttpResponseForbidden, HttpResponseBadRequest
from django.urls import reverse
from django.views.generic import FormView
from django.views.generic.detail import SingleObjectMixin
from django.shortcuts import render, render_to_response, redirect, get_object_or_404

from apps.home.forms.form import *

from django import forms
from django.template import RequestContext

from planificacion.models import *

import openpyxl
from django.db import IntegrityError
from django.core.exceptions import ValidationError

from django.core.paginator import Paginator
from django.views.decorators.csrf import csrf_exempt

from django.db.models import Q
from django.contrib.auth.models import User,Group,Permission
from django.http import JsonResponse

#from django.shortcuts import get_object_or_404, redirect, render, reverse

class index(SingleObjectMixin, FormView):
    form_class = MyForm
    initial = {'key': 'value'}
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            # <process form cleaned data>
            return HttpResponseRedirect('/success/')

        return render(request, self.template_name, {'form': form})



# Create your views here.
def import_data(request):
    message = ''
    status = None
    if "GET" == request.method:
        status = None
        #return render(request, 'index.html', {})
    if "POST" == request.method:
        user = User.objects.get(id=request.user.id)
        try:
            excel_file = request.FILES["excel_file"]

            # you may put validations here to check extension or file size

            wb = openpyxl.load_workbook(excel_file)
            #worksheet = wb.get_sheet_by_name('Hoja1')
            worksheet = wb.active

            # getting a particular sheet by name out of many sheets

            excel_data = list()
            data = list()
            # iterating over the rows and
            # getting value from each cell in row
            import datetime
            import time
            import re
        
            i = 2
            for row in worksheet.iter_rows():

                row_data = list()

                fecha_carga = worksheet['A'+str(i)].value
                if fecha_carga != None:

                    fecha_carga = worksheet['A'+str(i)].value
                    fecha_carga = datetime.datetime.strptime(str(fecha_carga), '%Y-%m-%d %H:%M:%S')
                    numero_carga = worksheet['B'+str(i)].value
                    ruta_codigo = worksheet['C'+str(i)].value
                    
                    ruta_codigo = ruta_codigo
                    ruta_codigo = re.sub('"', '', ruta_codigo)
                    ruta_codigo = re.sub('=', '', ruta_codigo)
                    try:
                        PlanificacionUsuarioRuta.objects.get(ruta_codigo=ruta_codigo)
                        objRuta = PlanificacionRuta.objects.get(codigo=ruta_codigo)
                    except Exception as e:
                        try:
                            objRuta = PlanificacionRuta.objects.get(codigo=ruta_codigo)
                            if objRuta:
                                PlanificacionUsuarioRuta.objects.create(ruta_codigo=objRuta,usuario=user)
                        except Exception as e:
                            objRuta = PlanificacionRuta.objects.create(codigo=ruta_codigo)
                            PlanificacionUsuarioRuta.objects.create(ruta_codigo=objRuta,usuario=user)
                        

                    destino = worksheet['D'+str(i)].value
                    local = worksheet['E'+str(i)].value
                    paradas = worksheet['F'+str(i)].value
                    numero_lpn = worksheet['G'+str(i)].value
                    peso = str(worksheet['H'+str(i)].value)
                    volumen = worksheet['I'+str(i)].value
                    guia_remision = worksheet['J'+str(i)].value
                    transportista = worksheet['K'+str(i)].value
                    fecha_llegada = worksheet['L'+str(i)].value
                    fecha_llegada = datetime.datetime.strptime(str(fecha_llegada), '%Y-%m-%d %H:%M:%S')
                    region = worksheet['M'+str(i)].value
                    #numero_placa = worksheet['N'+str(i)].value
                    #numero_placa = re.sub('-', '', numero_placa)
                    now = datetime.datetime.now()
                    PlanificacionCargaBulto.objects.create(user=user,fecha_carga=fecha_carga,numero_carga=numero_carga,
                        ruta_codigo=objRuta,destino=destino,local=local,paradas=paradas,numero_lpn=numero_lpn,
                        peso=peso,volumen=volumen,guia_remision=guia_remision,transportista=transportista,
                        fecha_llegada=fecha_llegada,region=region,fecha_registro=now)
                i=i+1

                #for cell in row:
                #    row_data.append(str(cell.value))
                #excel_data.append(row_data)
            status = True
            message = 'El archivo se cargo correctamente.'
            #return render(request, 'list.html', {'contacts': contacts})

        except IntegrityError as e: 
            status = False
            message = 'Lo sentimos, problemas al cargar el archivo. ' + str(e)
        except ValidationError as e:
            status = False
            message = 'Lo sentimos, problemas al cargar el archivo. ' + str(e)
        except ValueError as e:
            status = False
            message = 'Lo sentimos, problemas al cargar el archivo. ' + str(e)
        except Exception as e:
            status = False
            message = 'Lo sentimos, problemas al cargar el archivo. ' + str(e)

    mCargaBulto = PlanificacionCargaBulto.objects.all()
    paginator = Paginator(mCargaBulto, 20) # Show 25 contacts per page

    page = request.GET.get('page')
    data = paginator.get_page(page)

    return render(request, 'index.html', {"data":data,"message":message,"status":status})

@csrf_exempt
def eliminar_carga(request):
    import json
    message = ''
    success = True
    data = request.POST.getlist('data[]')

    try:
        i = 0
        while i < len(data):
            PlanificacionCargaBulto.objects.filter(id=data[i]).delete()
            #PlanificacionCargaBulto.objects.create(id=datas[i].get('id'))
            i = i + 1
        message = str(i) + ' Registro(s) eliminado(s).'
    except PlanificacionCargaBulto.DoesNotExist:
        message = 'Error al eliminar.'
        success = False
    except ValidationError as e: 
        success = False
        message = 'Lo sentimos, problemas al cargar el archivo. ' + str(e)

    return render(request, 'index.html', {"message":message,"status":success})

@csrf_exempt
def crear_geocercas(request):
    import json
    message = ''
    success = True
    form = planificacionGeocercaForm()
    if "POST" == request.method:

        codigo = request.POST.get('codigo')
        nombre = request.POST.get('nombre')
        direccion = request.POST.get('direccion')
        tipo_geocerca = request.POST.get('tipo_geocerca')
        coordenadas = request.POST.get('coordenadas')
        coordenadas_poligon = request.POST.get('coordenadas_poligon')

        if coordenadas:
            c = json.loads(coordenadas)
            try:
                
                form = planificacionGeocercaForm(request.POST)
                message = form.errors
                if form.is_valid():
                    #form.save()
                    mTG = PlanificacionTipoGeocerca.objects.get(id=tipo_geocerca)
                    mPG = PlanificacionGeocerca.objects.create(codigo=codigo,
                        nombre=nombre,direccion=direccion,
                        tipo_geocerca=mTG,
                        coordenadas=coordenadas,
                        coordenadas_poligon=coordenadas_poligon)
                    i = 0
                    while i < len(c):
                        PlanificacionDetalleGeocerca.objects.create(geocerca_id=mPG.id,latitud=c[i]['lat'],longitud=c[i]['lng'])
                        i = i + 1
                    message = 'La Geocerca se creo correctamente.'
                    return redirect('/listar_geocercas/')
            except PlanificacionGeocerca.DoesNotExist:
                message = 'Error tabla no existe.'
                success = False
            except ValidationError as e:
                success = False
                message = 'Lo sentimos, problemas al registrar.' + str(e)
            except IntegrityError as e: 
                success = False
                message = 'Lo sentimos, problemas al registrar. ' + str(e)
    return render(request, 'geocercas.html', {"message":message,"status":success,"form":form})

def listar_geocercas(request):
    message = ''
    status = None
    dataGeocerca = PlanificacionGeocerca.objects.all()

    paginator = Paginator(dataGeocerca, 20) # Show 25 contacts per page

    page = request.GET.get('page')
    data = paginator.get_page(page)

    return render(request, 'listar_geocercas.html', {"data":data,"message":message,"status":status})

@csrf_exempt
def eliminar_geocercas(request):
    import json
    message = ''
    status = False
    data = request.POST.getlist('data[]')

    try:
        i = 0
        while i < len(data):
            PlanificacionGeocerca.objects.filter(id=data[i]).delete()
            #PlanificacionCargaBulto.objects.create(id=datas[i].get('id'))
            i = i + 1
        message = str(i) + ' Registro(s) eliminado(s).'
        status = True
    except PlanificacionGeocerca.DoesNotExist:
        message = 'Lo sentimos, problemas al eliminar.'
        status = False
    except IntegrityError as e: 
        status = False
        message = 'Lo sentimos, problemas al eliminar. ' + str(e)

    dataGeocerca = PlanificacionGeocerca.objects.all()

    paginator = Paginator(dataGeocerca, 20) # Show 25 contacts per page

    page = request.GET.get('page')
    data = paginator.get_page(page)

    return render(request, 'listar_geocercas.html', {"data":data,"message":message,"status":status})

    #return redirect('/listar_geocercas/')
    #return render(request, 'listar_geocercas.html', {"message":message,"status":success})


@csrf_exempt
def edita_geocercas(request, id=None, template_name='editar_geocercas.html'):
    if id:
        geocerca = get_object_or_404(PlanificacionGeocerca, pk=id)

        if not geocerca.id:
            return HttpResponseForbidden()
    #else:
    #    article = Article(author=request.user)

    form = planificacionGeocercaForm(request.POST or None, instance=geocerca)
    c = PlanificacionDetalleGeocerca.objects.filter(geocerca_id=id)

    coordenadas = ''
    coordenadas_poligon = ''
    i = 0
    while i < len(c):
        coordenadas += '{ "lat": '+c[i].latitud+', "lng": '+c[i].longitud+' }, '
        coordenadas_poligon += c[i].latitud+' '+c[i].longitud+', '
        i = i + 1

    cp = coordenadas_poligon[0 : -2]
    coordenadas_poligon = '('+geocerca.coordenadas_poligon+')'

    if "POST" == request.method:
        if request.POST and form.is_valid():
            form.save()

            # Save was successful, so redirect to another page
            redirect_url = reverse(geocerca_save_success)
            return redirect(redirect_url)

    return render(request, template_name, {
        'form': form, 'id':id,'coordenadas_poligon':coordenadas_poligon
    })

@csrf_exempt
def editar_geocercas(request, template_name='editar_geocercas.html'):
    import json
    id = request.POST.get('id')
    if id:
        geocerca = get_object_or_404(PlanificacionGeocerca, pk=id)
        if not geocerca.id:
            return HttpResponseForbidden()
    #else:
    #    article = Article(author=request.user)

    form = planificacionGeocercaForm(request.POST or None, instance=geocerca)

    coordenadas = request.POST.get('coordenadas')
    coordenadas_poligon = request.POST.get('coordenadas_poligon')
    print(coordenadas)
    if coordenadas:

        if "POST" == request.method:
            if request.POST and form.is_valid():
                form.save()

                mG = PlanificacionGeocerca.objects.get(id=id)
                mG.coordenadas_poligon = coordenadas_poligon
                mG.save()

                c = json.loads(coordenadas)
                try:
                        mPG = PlanificacionDetalleGeocerca.objects.filter(geocerca_id=id)
                        m = 0
                        while m < len(mPG):
                            PlanificacionDetalleGeocerca.objects.get(id=mPG[m].id).delete()
                            m = m + 1
                        i = 0
                        while i < len(c):
                            PlanificacionDetalleGeocerca.objects.create(geocerca_id=geocerca.id,latitud=c[i]['lat'],longitud=c[i]['lng'])
                            i = i + 1

                        message = 'La Geocerca se creo correctamente.'
                        return redirect('/listar_geocercas/')
                except PlanificacionGeocerca.DoesNotExist:
                    message = 'Error tabla no existe.'
                    success = False
                except ValidationError as e:
                    success = False
                    message = 'Lo sentimos, problemas al registrar.' + str(e)
                except IntegrityError as e: 
                    success = False
                    message = 'Lo sentimos, problemas al registrar. ' + str(e)

                # Save was successful, so redirect to another page
                return redirect('/listar_geocercas/', product_id=True)

    return render(request, template_name, {
        'form':form
    })

def puntos_control(request):
    message = ''
    status = None
    dataRuta = PlanificacionRuta.objects.all()

    paginator = Paginator(dataRuta, 20) # Show 25 contacts per page

    page = request.GET.get('page')
    data = paginator.get_page(page)

    return render(request, 'puntos_control.html', {"data":data,"message":message,"status":status})

def point_in_poly(x,y,poly):
    """Return True if (x, y) lies within poly and False otherwise.
 
poly := [ (x1, y1), (x2, y2), (x3, y3), ... (xn, yn) ]
This uses the ray-casting algorithm for determining whether a
point is inside a polygon.
http://geospatialpython.com/2011/01/point-in-polygon.html
http://en.wikipedia.org/wiki/Point_in_polygon#Ray_casting_algorithm """
 
 
    n = len(poly)
    inside = False
 
    p1x,p1y = poly[0]
    for i in range(n+1):
        p2x,p2y = poly[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xints:
                        inside = not inside
        p1x,p1y = p2x,p2y
 
    return inside

@csrf_exempt
def consulta_registros(request):
    from django.db.models import Prefetch
    numero_carga = request.GET.get('numero_carga')
    numero_bandeja = request.GET.get('numero_bandeja')
    ruta = request.GET.get('ruta')
    local = request.GET.get('local')
    fecini = request.GET.get('fecini')
    fecfin = request.GET.get('fecfin')

    data_pc = {}
    countcargainicial = {}
    resum = []
    detalles = {}
    detallado = {}
    message = ''
    status = False
    data = []
    resumen = {}
    data2 = []
    control = []

    if "GET" == request.method:
        from django.db.models import Count
        from shapely import wkt
        try:
            dataPuntoControl = PlanificacionPuntoControl.objects.all().select_related('geocerca').select_related('tipo_control').select_related('ruta_codigo').order_by('-orden')
            data_pc = PlanificacionPuntoControl.objects.values('ruta_codigo').annotate(total=Count('ruta_codigo')).order_by('-total').first()

            countcargainicial = PlanificacionCargaBulto.objects.all().count()
            '''resumen = PlanificacionPuntoControl.objects.raw("select distinct t1.numero_lpn,t3.id,t3.nombre, "
                "t3.id, t1.ruta_codigo, t2.coordenadas_poligon point, t4.coordenadas_poligon polygon "
                "from planificacion_carga_bulto t1 "
                "inner join planificacion_carga_punto_control t2 on t1.numero_lpn = t2.numero_lpn "
                "inner join planificacion_punto_control t3 on t1.ruta_codigo = t3.ruta_codigo "
                "inner join planificacion_geocerca t4 on t3.id = t4.punto_control_id order by t1.numero_lpn")'''
            '''detalles = PlanificacionPuntoControl.objects.raw("select distinct t1.numero_lpn, t3.id, t3.nombre, t2.fecha_registro, t1.numero_carga, "
                "(select count(*) from planificacion_punto_control where ruta_codigo = t1.ruta_codigo ) numero_controles, "
                "t1.ruta_codigo, t1.local, t1.fecha_carga, "
                "t4.coordenadas_poligon polygon, t2.coordenadas_poligon point "
                "from planificacion_carga_bulto t1 "
                "left join planificacion_carga_punto_control t2 on t2.numero_lpn = t1.numero_lpn "
                "inner join planificacion_punto_control t3 on t1.ruta_codigo = t3.ruta_codigo "
                "inner join planificacion_geocerca t4 on t3.geocerca_id = t4.id "
                "where t3.ruta_codigo = %s "
                "or t1.numero_carga = %s "
                "or t1.numero_lpn = %s "
                "or t1.local = %s "
                "or (t1.fecha_carga BETWEEN convert(datetime2, %s, 103) AND convert(datetime2, %s, 103)) order by t1.numero_carga", [ruta,numero_carga,numero_bandeja,local,fecini,fecfin])'''
                
            detalles = PlanificacionCargaBulto.objects.raw("select t1.*, "
                "(select count(*) from planificacion_punto_control where ruta_codigo = t1.ruta_codigo ) numero_controles, "
                "t1.ruta_codigo, t1.local, t1.fecha_carga, "
                "t2.coordenadas_poligon point "
                "from planificacion_carga_bulto t1 "
                "left join planificacion_carga_punto_control t2 on t2.numero_lpn = t1.numero_lpn "
                "where t1.ruta_codigo = %s "
                "or t1.numero_carga = %s "
                "or t1.numero_lpn = %s "
                "or t1.local = %s "
                "or (t1.fecha_carga BETWEEN convert(datetime2, %s, 103) AND convert(datetime2, %s, 103)) order by t1.numero_carga", [ruta,numero_carga,numero_bandeja,local,fecini,fecfin])

            from collections import Counter
            from shapely import wkt
            import math
            lpn1 = []
            '''m = 0
            while m < len(detalles):
                detalles[m].totales = 0
                detalles[m].distance = 0
                
                if detalles[m].polygon and detalles[m].point:
                    poly = wkt.loads('POLYGON('+str(detalles[m].polygon)+')')
                    pt = wkt.loads('POINT'+str(detalles[m].point)+'')
                    distance = poly.distance(pt)  # 0.0
                    detalles[m].distance = distance
                    if distance == 0.0:
                        lpn1.append(detalles[m].numero_lpn)
                        detalles[m].totales = int(detalles[m].totales + 1)
                        resum.append(detalles[m])
                m = m + 1

            resumen = dict(Counter(resum))
            #for key,value in resumen.items():
            #resum = resumen + a
            print(resumen)'''

            #------------ DETALLE-----------------#
            numCarga = []
            y = 0
            while y < len(detalles):
                detalles[y].display = 'table-row'
                if not detalles[y].numero_carga in numCarga:
                    numCarga.append(detalles[y].numero_carga)
                    data2.append(detalles[y])
                else:
                    detalles[y].display = 'none'
                    data2.append(detalles[y])
                y = y + 1


            lpn = []
            lpn2 = []
            n = 0
            while n < len(detalles):
                m = 0
                detalles[n].x = ''
                while m < len(dataPuntoControl):
                    if detalles[n].ruta_codigo == dataPuntoControl[m].ruta_codigo:
                        if dataPuntoControl[m].geocerca != None:
                            try:
                                poly = wkt.loads('POLYGON('+str(dataPuntoControl[m].geocerca.coordenadas_poligon)+')')
                                pt = wkt.loads('POINT'+str(detalles[n].point)+'')
                                distance = poly.distance(pt)  # 0.0
                                if distance == 0.0:
                                    print('yyyyyyy')
                                    print(detalles[n].numero_lpn)
                                    detalles[n].x = 'x'
                                    setattr(detalles[n], "orden", dataPuntoControl[m].orden)
                                    setattr(detalles[n], "control_"+str(dataPuntoControl[m].orden), 'x')
                                    data.append(detalles[n])
                                else:
                                    setattr(detalles[n], "orden", dataPuntoControl[m].orden)
                                    setattr(detalles[n], "control_"+str(dataPuntoControl[m].orden), '')
                                    data.append(detalles[n])
                            except Exception as e:
                                setattr(detalles[n], "orden", dataPuntoControl[m].orden)
                                setattr(detalles[n], "control_"+str(dataPuntoControl[m].orden), '')
                                data.append(detalles[n])
                        #else:
                        #    setattr(detalles[n], "control_"+str(m), '')
                        #    data.append(detalles[n])
                    m = m + 1
                n = n + 1

            '''lpn = []
            lpn2 = []
            n = 0
            while n < len(detalles):
                detalles[n].distance = 0
                detalles[n].x = 0
                if detalles[n].polygon and detalles[n].point:
                    poly = wkt.loads('POLYGON('+str(detalles[n].polygon)+')')
                    pt = wkt.loads('POINT'+str(detalles[n].point)+'')
                    distance = poly.distance(pt)  # 0.0
                    detalles[n].distance = distance
                    if distance == 0.0:
                        if not detalles[n].numero_lpn in lpn2:
                            lpn2.append(detalles[n].numero_lpn)
                            detalles[n].x = int(detalles[n].x + 1)
                            data.append(detalles[n])

                else:
                    if not detalles[n].numero_lpn in lpn2:
                        lpn2.append(detalles[n].numero_lpn)
                        data.append(detalles[n])
                n = n + 1'''


            
            '''numCarga = []
            y = 0
            while y < len(data):
                data[y].display = 'table-row'
                if not data[y].numero_carga in numCarga:
                    numCarga.append(data[y].numero_carga)
                    data2.append(data[y])
                else:
                    data[y].display = 'none'
                    data2.append(data[y])
                y = y + 1'''
            #--------------FIN DETALLE-----------#
            message = ''
            status = True
        except Exception as e:
            message = e
            status = False

    return render(request, 'consulta_registros.html', {"data_pc":data_pc,
        "countcargainicial":countcargainicial,
        "resumen":resumen,
        "resum":resum,
        "detalles":data2,
        "data":data,
        "message":message,
        "status":status,
        "numero_carga": numero_carga,
        "numero_bandeja": numero_bandeja,
        "ruta": ruta,
        "local": local,
        "fecini": fecini,
        "fecfin": fecfin})

@csrf_exempt
def export_consulta_registros(request):
    from django.db.models import Prefetch
    numero_carga = request.GET.get('numero_carga')
    numero_bandeja = request.GET.get('numero_bandeja')
    ruta = request.GET.get('ruta')
    local = request.GET.get('local')
    fecini = request.GET.get('fecini')
    fecfin = request.GET.get('fecfin')

    data_pc = {}
    countcargainicial = {}
    detalles = {}
    data = []
    resum = []

    if "GET" == request.method:
        from shapely import wkt
        
        data_pc = PlanificacionPuntoControl.objects.all().order_by('id')
        countcargainicial = PlanificacionCargaBulto.objects.all().count()
        '''resumen = PlanificacionPuntoControl.objects.raw("select distinct t1.numero_lpn,t3.id,t3.nombre, "
            "t3.id, t1.ruta_codigo, t2.coordenadas_poligon point, t4.coordenadas_poligon polygon "
            "from planificacion_carga_bulto t1 "
            "inner join planificacion_carga_punto_control t2 on t1.numero_lpn = t2.numero_lpn "
            "inner join planificacion_punto_control t3 on t1.ruta_codigo = t3.ruta_codigo "
            "inner join planificacion_geocerca t4 on t3.id = t4.punto_control_id order by t1.numero_lpn")'''
        detalles = PlanificacionPuntoControl.objects.raw("select distinct t1.numero_lpn, t3.id, t3.nombre, t2.fecha_registro, t1.numero_carga, "
            "(select count(*) from planificacion_punto_control where ruta_codigo = t1.ruta_codigo ) numero_controles, "
            "t1.ruta_codigo, t1.local, t1.fecha_carga, "
            "t4.coordenadas_poligon polygon, t2.coordenadas_poligon point "
            "from planificacion_carga_bulto t1 "
            "left join planificacion_carga_punto_control t2 on t2.numero_lpn = t1.numero_lpn "
            "inner join planificacion_punto_control t3 on t1.ruta_codigo = t3.ruta_codigo "
            "inner join planificacion_geocerca t4 on t3.geocerca_id = t4.id "
            "where t3.ruta_codigo = %s "
            "or t1.numero_carga = %s "
            "or t1.numero_lpn = %s "
            "or t1.local = %s "
            "or (t2.fecha_registro BETWEEN convert(datetime2, %s, 103) AND convert(datetime2, %s, 103)) order by t1.numero_lpn", [ruta,numero_carga,numero_bandeja,local,fecini,fecfin])

        from collections import Counter
        from shapely import wkt
        import math
        lpn1 = []
        m = 0
        while m < len(detalles):
            detalles[m].totales = 0
            detalles[m].distance = 0
            
            if detalles[m].polygon and detalles[m].point:
                poly = wkt.loads('POLYGON('+str(detalles[m].polygon)+')')
                pt = wkt.loads('POINT'+str(detalles[m].point)+'')
                distance = poly.distance(pt)  # 0.0
                detalles[m].distance = distance
                if distance == 0.0:
                    lpn1.append(detalles[m].numero_lpn)
                    detalles[m].totales = int(detalles[m].totales + 1)
                    resum.append(detalles[m])
            m = m + 1

        resumen = dict(Counter(resum))
        print('ppppppp')
        print(resumen)

        lpn = []
        n = 0
        while n < len(detalles):
            detalles[n].distance = 0
            detalles[n].x = 0
            if detalles[n].polygon and detalles[n].point:
                poly = wkt.loads('POLYGON('+str(detalles[n].polygon)+')')
                pt = wkt.loads('POINT'+str(detalles[n].point)+'')
                distance = poly.distance(pt)  # 0.0
                detalles[n].distance = distance
                if distance == 0.0:
                    if not detalles[n].numero_lpn in lpn:
                        lpn.append(detalles[n].numero_lpn)
                        detalles[n].x = int(detalles[n].x + 1)
                        data.append(detalles[n])
            else:
                if not detalles[n].numero_lpn in lpn:
                    lpn.append(detalles[n].numero_lpn)
                    data.append(detalles[n])
            n = n + 1

        # Export excel
        from openpyxl import Workbook

        wb = Workbook()
        ws = wb.active

        #--------------RESUMEN---------------
        ws['A1'] = '-'
        ws['B1'] = 'Control 0'
        ws['A2'] = 'Total'
        ws['A3'] = 'Dentro de Geocerca'
        
        cr = 3
        for control in data_pc:
            ws.cell(row=1,column=cr).value = control.nombre
            cr = cr + 1

        ws.cell(row=2,column=2).value = countcargainicial
        
        for control in data_pc:
            x = 3
            for key,value in resumen.items():
                if key.id == control.id:
                    ws.cell(row=2,column=x).value = value
                x = x + 1

        for control in data_pc:
            x = 3
            for key,value in resumen.items():
                if key.id == control.id:
                    ws.cell(row=3,column=x).value = value
                x = x + 1
        

        #--------------DETALLE................
        ws['A5'] = 'Número de carga'
        ws['B5'] = 'Número de bandeja'
        ws['C5'] = '#Controles'
        ws['D5'] = 'Ruta'
        ws['E5'] = 'Local'
        ws['F5'] = 'Fecha de carga'

        c = 7
        for control in data_pc:
            ws.cell(row=5,column=c).value = control.nombre
            c = c + 1

        cont=6
        for row in data:
            ws.cell(row=cont,column=1).value = row.numero_carga
            ws.cell(row=cont,column=2).value = row.numero_lpn
            ws.cell(row=cont,column=3).value = row.numero_controles
            ws.cell(row=cont,column=4).value = row.ruta_codigo.codigo
            ws.cell(row=cont,column=5).value = row.local
            ws.cell(row=cont,column=6).value = row.fecha_carga
            column = 7
            for control in data_pc:
                if row.id == control.id:
                    if row.x == 1:
                        ws.cell(row=cont,column=column).value = 'x'
                    else:
                        ws.cell(row=cont,column=column).value = ''
                else:
                    ws.cell(row=cont,column=column).value = ''
                column = column + 1
            cont = cont + 1
        nombre_archivo ="ListadoFormasPago.xlsx" 
        response = HttpResponse(content_type="application/ms-excel") 
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response 

@csrf_exempt
def registro_manual(request):
    from django.db.models import Prefetch
    numero_carga = request.POST.get('numero_carga')
    numero_bandeja = request.POST.get('numero_bandeja')
    fecini = request.POST.get('fecini')
    fecfin = request.POST.get('fecfin')
    message = ''
    status = None
    data_pc = {}
    countcargainicial = {}
    resumen = {}
    detalles = {}
    detallado = {}
    data = []

    if "POST" == request.method:
        from shapely import wkt
        
        detalles = PlanificacionPuntoControl.objects.raw("select distinct t1.numero_lpn, t3.id, t3.nombre, t2.fecha_registro, t1.numero_carga, "
            "(select count(*) from planificacion_punto_control where ruta_codigo = t1.ruta_codigo ) numero_controles, "
            "t1.ruta_codigo, t1.local, t1.fecha_carga, "
            "t4.coordenadas_poligon polygon, t2.coordenadas_poligon point "
            "from planificacion_carga_bulto t1 "
            "left join planificacion_carga_punto_control t2 on t2.numero_lpn = t1.numero_lpn "
            "inner join planificacion_punto_control t3 on t1.ruta_codigo = t3.ruta_codigo "
            "inner join planificacion_geocerca t4 on t3.geocerca_id = t4.id "
            "where t1.numero_carga = %s "
            "or t1.numero_lpn = %s "
            "or (t1.fecha_carga BETWEEN convert(datetime2, %s, 103) AND convert(datetime2, %s, 103)) order by t1.numero_lpn", [numero_carga,numero_bandeja,fecini,fecfin])

        from shapely import wkt
        lpn = []
        n = 0
        while n < len(detalles):
            detalles[n].distance = 0
            detalles[n].x = 0
            if detalles[n].point:
                #poly = wkt.loads('POLYGON('+str(detalles[n].polygon)+')')
                #pt = wkt.loads('POINT'+str(detalles[n].point)+'')
                #distance = poly.distance(pt)  # 0.0
                #detalles[n].distance = distance
                #if distance == 0.0:
                #    if not detalles[n].numero_lpn in lpn:
                #        lpn.append(detalles[n].numero_lpn)
                #        detalles[n].x = int(detalles[n].x + 1)
                #        data.append(detalles[n])
                print(detalles[n].point)
            else:
                if not detalles[n].numero_lpn in lpn:
                    lpn.append(detalles[n].numero_lpn)
                    data.append(detalles[n])
            n = n + 1

        message = ''
        status = None

    return render(request, 'registro_manual.html', {
        "detalles":data,
        "message":message,
        "status":status})

@csrf_exempt
def registro_manual_scaner(request):
    message = ''
    status = False
    data = {}
    numero_placa = request.GET.get('numero_placa')
    numero_lpn = request.GET.get('numero_bandeja')
    latitud = request.GET.get('latitud')
    longitud = request.GET.get('longitud')
    fecha_registro = request.GET.get('fecha_registro')

    import datetime
    import time
    now = datetime.datetime.now()
    fecha_registro = datetime.datetime.strptime(str(fecha_registro), '%Y-%m-%d %H:%M:%S')

    try:
        coordenadas = '{"lat": '+latitud+', "lng": '+longitud+'}'
        coordenadas_poligon = '('+latitud+' '+longitud+')'
        PlanificacionCargaPuntoControl.objects.create(numero_lpn=numero_lpn,
            numero_placa=numero_placa,fecha_registro=fecha_registro,
            latitud=latitud,longitud=longitud,
            fecha_envio=now,coordenadas=coordenadas,coordenadas_poligon=coordenadas_poligon)
        status = True
        message = 'Registro agregado correctamente.'
    except Exception as e:
        message = 'Error al insertar registro. ' + str(e)

    #return redirect('/registro_manual/')
    data = {'status': status, 'message': message}
    return JsonResponse(data)
    #return render(request, 'registro_manual.html', {"message":message,"status":status})

@csrf_exempt
def consulta_incidencias(request):
    message = ''
    status = None
    data = {}
    from django.db.models import Prefetch
    ruta = request.POST.get('ruta')
    transportista = request.POST.get('transportista')
    numero_placa = request.POST.get('numero_placa')
    fecini = request.POST.get('fecini')
    fecfin = request.POST.get('fecfin')

    try:
        if "POST" == request.method:
            '''data = PlanificacionIncidencia.objects.raw(
                "select distinct t2.id, t1.user_id, t2.fecha_registro, t2.ruta, t2.descripcion from planificacion_carga_bulto t1 "
                "inner join planificacion_incidencia t2 on t1.ruta_codigo = t2.ruta "
                "inner join auth_user t3 on t1.user_id = t3.id "
                "where t2.numero_placa = %s or t2.ruta = %s "
                "or (t2.fecha_registro BETWEEN convert(datetime2, %s, 103) AND convert(datetime2, %s, 103)) "
                "or t3.username like %s ", [numero_placa, ruta, fecini, fecfin, transportista])'''
            data = PlanificacionIncidencia.objects.raw(
                "select distinct t2.id, t3.username transportista, t2.fecha_registro, t2.ruta, t2.descripcion, t3.username from planificacion_ruta t1 "
                "right join planificacion_incidencia t2 on t1.codigo = t2.ruta "
                "left join auth_user t3 on t2.usuario_id = t3.id "
                "where t2.numero_placa = %s or t2.ruta = %s "
                "or (t2.fecha_registro BETWEEN convert(datetime2, %s, 103) AND convert(datetime2, %s, 103)) "
                "or t3.username like %s ", [numero_placa, ruta, fecini, fecfin, transportista])

    except Exception as e:
        print(e)
        message = ''
        status = None

    return render(request, 'consulta_incidencias.html', {"data":data,"message":message,"status":status})